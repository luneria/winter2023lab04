public class WashingMachine
{
	private String brand;
	private String color;
	private int capacity;
	
	public void HowManyDirtyClothesYouHave()
	{
		
	}
	
	public void VeryFancyBrand()
	{
		System.out.println(brand);
	}
	
	public void modifyColor(String typeColor)
	{
		if(isValid(typeColor))
		{
			this.color = typeColor;
			System.out.println("Hourray !!! Your washing Machine is now Red:)");
		}
		else
		{
			System.out.println("Please pick one of this colors: red, blue, green or yellow");
		}
	}
	
	private boolean isValid(String usersColor)
	{
		boolean isValidColor = (usersColor.equals("red") || usersColor.equals("blue") || usersColor.equals("green") || usersColor.equals("yellow"));
		return isValidColor;
	}
	
	//Setters methods
	public String getBrand()
	{
		return this.brand;
	}
	
	public String getColor()
	{
		return this.color;
	}
	
	public int getCapacity()
	{
		return this.capacity;
	}
	
	
	//Getters methods
	public void setBrand(String newBrand)
	{
		this.brand = newBrand;
	}

	public void setColor(String newColor)
	{
		this.color = newColor;
	}
	
	
	//Adding a constructor
	public WashingMachine(String brand, String color, int capacity)
	{
		this.brand = "Lg";
		this.color = "red";
		this.capacity = 20;	
	}
}
